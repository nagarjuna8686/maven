# maven

pipeline {
	
	agent any {
    
	stage('Checkout') {
          
		  checkout git-url
	    }
		  
	stage('compile') {
	      
		  def mvnHome = tool 'maven'
		  
		  bat (/"${mvnHome}\bin\mvn" compile/)
		}
		  
	stage('Code Analysis') {
	      
		  def mvnHome = tool 'maven'
		  
		  withSonarQubeEnv('sonar') {
		  
		  bat "${mvnHome}/bin/mvn sonar:sonar"
		  }
		}
		
	stage('Quality Gate Status Check') {
	      
		  timeout(time: 10, unit: 'MINUTES') {
		  
		  def qg = waitForQualityGate()
			 
			 if (qg != 'OK') {
			     
				 emailext attachLog: true, body: '''Hi,
			 
			 SonarQube Quality Gates Staus Faild.''', recipentProviders: [developers()]. subject: 'SoanrQube-Status', to: 'nagarjuna0907@gmail.com'
			     
				 error "Pipeline aborted due to quality gate failure: ${qg.status}"
				 }
			}
		}
		
	stage('Build') {
	      
		  def mvnHome = tool 'maven'
		  
		  bat (/"${mvnHome}\bin\mvn" clean install/)
        }
		
    stage('JUnit Testing') {
          
		  junit '**/target/surefire-reports/*.xml'
        }
		
    stage ('copy jar to deployments'){
		   
		   fileOperations([folderCopyOperation(destinationFolderPath: 'D://ProjectDeployment//', sourceFolderPath: 'C://Program Files (x86)//Jenkins//workspace//projectname//loan-origination//target//')])
		   
		   bat 'oc version'	
		}
			
	}
 }
 
*********************************Linux*******************************
pipeline {
	
	agent any {
    
	stage('Checkout') {
          
		  checkout git-url
	    }
		  
	stage('compile') {
	      
		  def mvnHome = tool name: 'maven3', type: 'maven'
		  
		  sh "${mvnHome}/bin/mvn compile"
		}
		  
	stage('Code Analysis') {
	      
		  def mvnHome = tool name: 'maven3', type: 'maven'
		  
		  withSonarQubeEnv('sonar') {
		  
		  sh "${mvnHome}/bin/mvn sonar:sonar"
		  }
		}
		
	stage('Quality Gate Status Check') {
	      
		  timeout(time: 10, unit: 'MINUTES') {
		  
		  def qg = waitForQualityGate()
			 
			 if (qg != 'OK') {
			     
				 emailext attachLog: true, body: '''Hi,
			 
			 SonarQube Quality Gates Staus Faild.''', recipentProviders: [developers()]. subject: 'SoanrQube-Status', to: 'nagarjuna0907@gmail.com'
			     
				 error "Pipeline aborted due to quality gate failure: ${qg.status}"
				 }
			}
		}
		
	stage('Build') {
	      
		  def mvnHome = tool name: 'maven3', type: 'maven'
		  
		  sh "${mvnHome}/bin/mvn clean package"
        }
		
    stage('JUnit Testing') {
          
		  junit '**/target/surefire-reports/*.xml'
        }
		
    stage ('Deploy to tomcat'){
		   
		   sh 'scp -o StrictHostKeyChecking=no /var/lib/jenkins/worksapce/projectname/target/*.jar ubuntu@192.168.33.12:/opt/tomcat/webapps/'
		   sh 'ssh -o StrictHostKeyChecking=no ubuntu@192.168.33.12:nohup java -jar /opt/tomcat/webapps/filename.jar'
		   
		}
			
	}
 }
          	